﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace ASP.NETCoreWebApplication1.Migrations
{
    /// <inheritdoc />
    public partial class AddInitialDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(80)", maxLength: 80, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(90)", maxLength: 90, nullable: false),
                    AccountType = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastLoginAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Units = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Moves",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    AccountId = table.Column<string>(type: "nvarchar(80)", maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Moves", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Moves_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Workouts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    AccountId = table.Column<string>(type: "nvarchar(80)", maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workouts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Workouts_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sets",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Weight = table.Column<decimal>(type: "decimal(6,3)", nullable: false),
                    Count = table.Column<int>(type: "int", nullable: false),
                    WeightCount = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    MoveId = table.Column<int>(type: "int", nullable: false),
                    WorkoutId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sets_Moves_MoveId",
                        column: x => x.MoveId,
                        principalTable: "Moves",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Sets_Workouts_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workouts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "Id", "AccountType", "CreatedAt", "Email", "LastLoginAt", "Units" },
                values: new object[,]
                {
                    { "test1", 0, new DateTime(2022, 8, 13, 12, 22, 9, 675, DateTimeKind.Utc).AddTicks(1265), "test001@email.com", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 },
                    { "test2", 1, new DateTime(2022, 11, 7, 12, 22, 9, 675, DateTimeKind.Utc).AddTicks(1274), "test002@email.com", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { "test3", 2, new DateTime(2022, 11, 21, 12, 22, 9, 675, DateTimeKind.Utc).AddTicks(1275), "test003@email.com", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 }
                });

            migrationBuilder.InsertData(
                table: "Moves",
                columns: new[] { "Id", "AccountId", "Name" },
                values: new object[,]
                {
                    { -6, "test3", "Dumbbell biceps curl" },
                    { -5, "test2", "Seated french press" },
                    { -4, "test2", "Chin-up" },
                    { -3, "test1", "Squat" },
                    { -2, "test1", "Deadlift" },
                    { -1, "test1", "Bench press" }
                });

            migrationBuilder.InsertData(
                table: "Workouts",
                columns: new[] { "Id", "AccountId", "EndTime", "Note", "StartTime" },
                values: new object[,]
                {
                    { -4, "test3", new DateTime(2022, 11, 21, 9, 22, 9, 675, DateTimeKind.Utc).AddTicks(1349), "Biceps", new DateTime(2022, 11, 21, 7, 22, 9, 675, DateTimeKind.Utc).AddTicks(1348) },
                    { -3, "test2", new DateTime(2022, 11, 21, 6, 22, 9, 675, DateTimeKind.Utc).AddTicks(1347), "Arms only", new DateTime(2022, 11, 21, 5, 22, 9, 675, DateTimeKind.Utc).AddTicks(1347) },
                    { -2, "test1", new DateTime(2022, 11, 20, 17, 22, 9, 675, DateTimeKind.Utc).AddTicks(1346), "Leg day", new DateTime(2022, 11, 20, 14, 22, 9, 675, DateTimeKind.Utc).AddTicks(1345) },
                    { -1, "test1", new DateTime(2022, 11, 21, 9, 22, 9, 675, DateTimeKind.Utc).AddTicks(1344), "Benchpress with Joe", new DateTime(2022, 11, 21, 7, 22, 9, 675, DateTimeKind.Utc).AddTicks(1343) }
                });

            migrationBuilder.InsertData(
                table: "Sets",
                columns: new[] { "Id", "Count", "MoveId", "Weight", "WorkoutId" },
                values: new object[,]
                {
                    { 1, 8, -1, 80m, -1 },
                    { 2, 8, -1, 82.5m, -1 },
                    { 3, 3, -1, 90m, -1 },
                    { 4, 6, -2, 120m, -2 },
                    { 5, 6, -2, 130m, -2 },
                    { 6, 5, -2, 130m, -2 },
                    { 7, 8, -3, 80m, -2 },
                    { 8, 8, -3, 90m, -2 },
                    { 9, 16, -4, 0m, -3 },
                    { 10, 8, -4, 20m, -3 },
                    { 11, 6, -4, 25m, -3 },
                    { 12, 12, -5, 20m, -3 },
                    { 13, 12, -5, 25m, -3 }
                });

            migrationBuilder.InsertData(
                table: "Sets",
                columns: new[] { "Id", "Count", "MoveId", "Weight", "WeightCount", "WorkoutId" },
                values: new object[,]
                {
                    { 14, 12, -6, 30m, 2, -4 },
                    { 15, 12, -6, 30m, 2, -4 },
                    { 16, 12, -6, 35m, 2, -4 },
                    { 17, 10, -6, 35m, 2, -4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Email",
                table: "Accounts",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Moves_AccountId",
                table: "Moves",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Sets_MoveId",
                table: "Sets",
                column: "MoveId");

            migrationBuilder.CreateIndex(
                name: "IX_Sets_WorkoutId",
                table: "Sets",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_Workouts_AccountId",
                table: "Workouts",
                column: "AccountId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sets");

            migrationBuilder.DropTable(
                name: "Moves");

            migrationBuilder.DropTable(
                name: "Workouts");

            migrationBuilder.DropTable(
                name: "Accounts");
        }
    }
}
