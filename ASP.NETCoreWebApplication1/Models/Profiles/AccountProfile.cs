﻿using ASP.NETCoreWebApplication1.Models.Domain;
using ASP.NETCoreWebApplication1.Models.DTOs.Account;
using AutoMapper;

namespace ASP.NETCoreWebApplication1.Models.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<Account, AccountReadDTO>().ReverseMap();

        }
    }
}