﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ASP.NETCoreWebApplication1.Models.Domain
{
    [Table("Sets")]
    public class Set
    {
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "decimal(6,3)")]
        public decimal Weight { get; set; }
        [Required]
        public int Count { get; set; }
        public int WeightCount { get; set; }
        public int MoveId { get; set; }
        public Move Move { get; set; }
        public int WorkoutId { get; set; }
        public Workout Workout { get; set; }
    }
}