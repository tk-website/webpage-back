﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ASP.NETCoreWebApplication1.Models.Domain
{
    [Table("Workouts")]
    public class Workout
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        [MaxLength(50)]
        public string Note { get; set; }
        public ICollection<Set> Sets { get; set; }
        [MaxLength(80)]
        public string AccountId { get; set; }
        public Account Account { get; set; }
    }
}