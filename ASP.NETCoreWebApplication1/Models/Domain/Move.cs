﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ASP.NETCoreWebApplication1.Models.Domain
{
    [Table("Moves")]
    public class Move
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(40)]
        public string Name { get; set; }
        [MaxLength(80)]
        public string AccountId { get; set; }
        public Account Account { get; set; }
    }
}