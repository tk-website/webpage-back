﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ASP.NETCoreWebApplication1.Models.Domain
{
    [Table("Accounts")]
    public class Account
    {
        // Primary key
        [MaxLength(80)]
        public string Id { get; set; }
        // Fields
        [MaxLength(90)]
        [Required]
        public string Email { get; set; }
        [Required]
        public int AccountType { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastLoginAt { get; set; }
        [Required]
        public int Units { get; set; }
        public ICollection<Workout> Workouts { get; set; }
        public ICollection<Move> Moves { get; set; }
    }
}