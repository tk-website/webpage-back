﻿namespace ASP.NETCoreWebApplication1.Models.DTOs.Account
{
    public class AccountCreateDTO
    {
        public string AccountId { get; set; }
        public string Email { get; set; }
        public string AccountType { get; set; }
    }
}