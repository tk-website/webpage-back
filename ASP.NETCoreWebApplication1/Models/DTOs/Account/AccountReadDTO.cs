﻿using ASP.NETCoreWebApplication1.Models.Domain;

namespace ASP.NETCoreWebApplication1.Models.DTOs.Account
{
    public class AccountReadDTO
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}