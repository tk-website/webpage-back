﻿using ASP.NETCoreWebApplication1.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace ASP.NETCoreWebApplication1.Context
{
    public class WorkoutDbContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Move> Moves { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Set> Sets { get; set; }

        public WorkoutDbContext(DbContextOptions<WorkoutDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .HasMany<Workout>(a => a.Workouts)
                .WithOne(w => w.Account);

            modelBuilder.Entity<Account>()
                .HasMany<Move>(a => a.Moves)
                .WithOne(m => m.Account);
            
            modelBuilder.Entity<Account>()
                .HasIndex(a => new { a.Email })
                .IsUnique(true);

            modelBuilder.Entity<Workout>()
                .HasMany<Set>(w => w.Sets)
                .WithOne(s => s.Workout);

            modelBuilder.Entity<Set>()
                .HasOne<Move>(s => s.Move);

            modelBuilder.Entity<Set>()
                .HasOne<Workout>(s => s.Workout)
                .WithMany(w => w.Sets)
                .HasForeignKey(s => s.WorkoutId)
                .OnDelete(DeleteBehavior.Restrict);
            
            modelBuilder.Entity<Set>()
                .Property(s => s.WeightCount)
                .HasDefaultValue(1);

            modelBuilder.Entity<Account>().HasData(
                new Account { AccountType = 0, Id = "test1", Email = "test001@email.com", Units = 0, CreatedAt = (DateTime.UtcNow - TimeSpan.FromDays(100)), },
                new Account { AccountType = 1, Id = "test2", Email = "test002@email.com", Units = 1, CreatedAt = (DateTime.UtcNow - TimeSpan.FromDays(14)) },
                new Account { AccountType = 2, Id = "test3", Email = "test003@email.com", Units = 0, CreatedAt = DateTime.UtcNow }
            );
            
            modelBuilder.Entity<Workout>().HasData(
                new Workout { Id = -1, AccountId  = "test1", Note = "Benchpress with Joe", StartTime = (DateTime.UtcNow - TimeSpan.FromHours(5)), EndTime = (DateTime.UtcNow - TimeSpan.FromHours(3))},
                new Workout { Id = -2,  AccountId = "test1", Note = "Leg day", StartTime = (DateTime.UtcNow - TimeSpan.FromHours(22)), EndTime = (DateTime.UtcNow - TimeSpan.FromHours(19))},
                new Workout { Id = -3,  AccountId = "test2", Note = "Arms only", StartTime = (DateTime.UtcNow - TimeSpan.FromHours(7)), EndTime = (DateTime.UtcNow - TimeSpan.FromHours(6))},
                new Workout { Id = -4,  AccountId = "test3", Note = "Biceps", StartTime = (DateTime.UtcNow - TimeSpan.FromHours(5)), EndTime = (DateTime.UtcNow - TimeSpan.FromHours(3))}
            );
            
            modelBuilder.Entity<Move>().HasData(
                new Move { Id = -1, AccountId = "test1", Name = "Bench press" },
                new Move { Id = -2, AccountId = "test1", Name = "Deadlift" },
                new Move { Id = -3, AccountId = "test1", Name = "Squat" },
                new Move { Id = -4, AccountId = "test2", Name = "Chin-up" },
                new Move { Id = -5, AccountId = "test2", Name = "Seated french press" },
                new Move { Id = -6, AccountId = "test3", Name = "Dumbbell biceps curl" }
            );

            modelBuilder.Entity<Set>().HasData(
                new Set { Id = 1, MoveId = -1, WorkoutId = -1, Weight = 80m, Count = 8 },
                new Set { Id = 2, MoveId = -1, WorkoutId = -1, Weight = 82.5m, Count = 8 },
                new Set { Id = 3, MoveId = -1, WorkoutId = -1, Weight = 90m, Count = 3 },
                new Set { Id = 4, MoveId = -2, WorkoutId = -2, Weight = 120m, Count = 6 },
                new Set { Id = 5, MoveId = -2, WorkoutId = -2, Weight = 130m, Count = 6 },
                new Set { Id = 6, MoveId = -2, WorkoutId = -2, Weight = 130m, Count = 5 },
                new Set { Id = 7, MoveId = -3, WorkoutId = -2, Weight = 80m, Count = 8 },
                new Set { Id = 8, MoveId = -3, WorkoutId = -2, Weight = 90m, Count = 8 },
                new Set { Id = 9, MoveId = -4, WorkoutId = -3, Weight = 0m, Count = 16 },
                new Set { Id = 10, MoveId = -4, WorkoutId = -3, Weight = 20m, Count = 8 },
                new Set { Id = 11, MoveId = -4, WorkoutId = -3, Weight = 25m, Count = 6 },
                new Set { Id = 12, MoveId = -5, WorkoutId = -3, Weight = 20m, Count = 12 },
                new Set { Id = 13, MoveId = -5, WorkoutId = -3, Weight = 25m, Count = 12 },
                new Set { Id = 14, MoveId = -6, WorkoutId = -4, Weight = 30m, Count = 12, WeightCount = 2},
                new Set { Id = 15, MoveId = -6, WorkoutId = -4, Weight = 30m, Count = 12, WeightCount = 2},
                new Set { Id = 16, MoveId = -6, WorkoutId = -4, Weight = 35m, Count = 12, WeightCount = 2},
                new Set { Id = 17, MoveId = -6, WorkoutId = -4, Weight = 35m, Count = 10, WeightCount = 2}
                );
        }
    }
}