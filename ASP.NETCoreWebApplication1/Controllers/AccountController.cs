﻿using System.Net.Mime;
using ASP.NETCoreWebApplication1.Context;
using ASP.NETCoreWebApplication1.Models.DTOs.Account;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ASP.NETCoreWebApplication1.Controllers
{
    [Route("api/accounts")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class AccountsController : ControllerBase
    {
        private readonly WorkoutDbContext _context;
        private readonly IMapper _mapper;

        public AccountsController(WorkoutDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Get all accounts.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AccountReadDTO>>> GetAccounts()
        {
            return _mapper.Map<List<AccountReadDTO>>(
                await _context.Accounts
                    .ToListAsync());
        }
    }
}